<?php
// +----------------------------------------------------------------------
// | HkCms api 中间件
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.hkcms.cn, All rights reserved.
// +----------------------------------------------------------------------
// | Author: HkCms team <admin@hkcms.cn>
// +----------------------------------------------------------------------

return [
    // 跨域中间件
    \think\middleware\AllowCrossDomain::class,
    // 语言包
    \app\common\middleware\LoadLangPack::class
];