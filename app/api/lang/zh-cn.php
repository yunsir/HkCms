<?php
return [
    // 提示
    "Operation completed"                       => '操作成功',
    "Operation failed"                          => '操作失败',
    "The mailbox interface is down"             => '邮箱接口已关闭',

    //
    'Email'                                     => '邮箱',
    'Event'                                     => '事件',
];