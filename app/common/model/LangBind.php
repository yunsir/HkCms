<?php
// +----------------------------------------------------------------------
// | HkCms 内容与语言关联模型
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2021 http://www.hkcms.cn, All rights reserved.
// +----------------------------------------------------------------------
// | Author: 广州恒企教育科技有限公司 <admin@hkcms.cn>
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace app\common\model;

use think\facade\Db;
use think\Model;

class LangBind extends Model
{

    /**
     * 获取语言绑定的ID
     * @param $value
     * @param string $table
     * @param string $lang
     * @return mixed|Db
     */
    public static function getBindValue($value, string $table, string $lang = '')
    {
        if (site('content_lang_on')!=1) {
            return $value;
        }

        $lang = empty($lang) ? app()->lang->getLangSet() : $lang;
        $l = Db::name($table)->where(['id'=>$value])->value('lang');
        if ($l==$lang) {
            return $value;
        }

        // 找出其他绑定的语言
        $data = Db::name('lang_bind')->where(['table'=>$table])->where(function ($query) use ($value) {
            $query->whereOr(['value_id'=>$value])->whereOr(['main_id'=>$value]);
        })->select();

        foreach ($data as $key=>$v) {
            if ($lang==$v['lang'] && $v['value_id']==0) {
                return $v['main_id'];
            } else if ($lang==$v['lang'] && $v['value_id']!=0) {
                return $v['value_id'];
            } else if ($lang!=$v['lang'] && $v['value_id']==$value) {
                return $v['main_id'];
            }
        }
        return $value;
    }

    /**
     * 获取关联的ID数组
     * @param string $table
     * @param int $curId
     * @return array
     */
    public static function contentGet(string $table, $curId)
    {
        $data = [];
        // 找出其他语言
        $info = Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId,'value_id'=>0])->find();
        if ($info) { // 主记录
            $data = Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId])->where('value_id','<>',0)->column('value_id');
        } else {
            $curId = Db::name('lang_bind')->where(['table'=>$table,'value_id'=>$curId])->value('main_id');
            if ($curId) {
                $data = Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId])->where('value_id','<>',0)->column('value_id');
            }
        }
        if (!empty($curId)) {
            $data[] = $curId;
        }
        return $data;
    }

    /**
     * 多语言内容关联添加
     * @param string $table 表格
     * @param array $data 表数据数组
     * @param array $lanField 追加初次新增的语言标识字段
     * @param bool $isBind 是否往语言绑定表里添加数据
     * @return array 返回新增参数给定的table表的ID数组
     */
    public static function contentAdd(string $table, array $data, array $lanField = [], bool $isBind = true)
    {
//        $curLang = app()->cache->get('admin_content_lang'); // 当前内容编辑模式
        $curLang = $data['lang']; // 当前内容编辑模式
        $mainId = $data['id'];
        if ($isBind) {
            Db::name('lang_bind')->insert([
                'main_id'=>$mainId,
                'value_id'=>0,
                'table'=>$table,
                'lang'=>$curLang,
                'create_time'=>time(),
            ]);
        }

        $langs = site('content_lang_list');
        if (empty($langs)) {
            $langs = \app\admin\model\routine\Config::where(['name'=>'content_lang'])->value('data_list');
            $langs = json_decode($langs, true);
        }

        unset($data['id']);
        $idArr = [];
        $tempData = $data;
        foreach ($langs as $k=>$value) {
            if ($curLang!=$k) {
                $data['lang'] = $k;
                foreach ($lanField as $f) {
                    if (!empty($data[$f])) {
                        $data[$f] = "[$k]".$tempData[$f];
                    }
                }

                if (isset($data['parent_id']) && $data['parent_id']>0) { // 有上下级的情况下
                    $tmpData = self::contentGet($table, $data['parent_id']); // 获取父级不同语言的ID
                    $data['parent_id'] = \think\facade\Db::name($table)->whereIn('id', $tmpData)->where(['lang'=>$k])->value('id');
                }

                $id = \think\facade\Db::name($table)->insertGetId($data);
                $idArr[] = $id;
                if ($isBind) {
                    \think\facade\Db::name('lang_bind')->insert([
                        'main_id'=>$mainId,
                        'value_id'=>$id,
                        'table'=>$table,
                        'lang'=>$k,
                        'create_time'=>time(),
                    ]);
                }
            }
        }
        return $idArr;
    }

    /**
     * 内容多语言关联删除
     * @param string $table 表格
     * @param integer $curId 当前操作的ID
     * @param bool $bl true-直接删除，false-回收站
     * @return array 返回删除的表主键
     */
    public static function contentDel(string $table, int $curId, bool $bl = true)
    {
        $data = [];
        // 找出其他语言
        $info = Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId,'value_id'=>0])->find();
        if ($info) { // 主记录
            $data = Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId])->where('value_id','<>',0)->column('value_id');

            if ($bl) {
                // 删除主记录相关的其他语言包
                Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId])->delete();
            }
        } else {
            $curId = Db::name('lang_bind')->where(['table'=>$table,'value_id'=>$curId])->value('main_id');
            if ($curId) {
                $data = Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId])->where('value_id','<>',0)->column('value_id');
                if ($bl) {
                    // 删除主记录相关的其他语言包
                    Db::name('lang_bind')->where(['table'=>$table,'main_id'=>$curId])->delete();
                }
            }
        }

        if (!empty($curId)) {
            $data[] = $curId;
        }
        if (!empty($data)) {
            if ($bl) {
                Db::name($table)->whereIn('id',$data)->delete();
            } else {
                Db::name($table)->whereIn('id',$data)->update(['delete_time'=>time()]);
            }
        }

        return $data;
    }
}