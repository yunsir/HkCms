<?php
// 中间件配置
return [
    // 别名或分组
    'alias'    => [
        // 表单令牌
        'form_token'=>\app\common\middleware\FormTokenCheck::class,
        // 后台
        'login'=>\app\admin\middleware\LoginCheck::class,
        'auth'=>\app\admin\middleware\Auth::class,
        // api
        'api_login'=>\app\api\middleware\LoginCheck::class,
        'api_auth'=>\app\api\middleware\Auth::class
    ],
    // 优先级设置，此数组中的中间件会按照数组中的顺序优先执行
    'priority' => [],
];
